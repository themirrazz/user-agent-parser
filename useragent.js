// Licensed under GNU GPL 3.0
// (C) Themirrazz 2022
// All rights reserved

var parser={
    /**
     * @param agent{string}
    */
    parse: function (agent) {
        var product=agent.split(" ")[0].split("/")[0].trim();
        var productVersion=agent.split(" ")[0].split("/")[1].trim();
        var systemInformation=[];
        var other=[];
        var renderer=[];
        if(agent.indexOf("(") > -1) {
            agent.slice(
                agent.indexOf("(")+1,
                agent.indexOf(")")
            ).split(";").forEach(chunk=>{
                systemInformation.push(chunk.trim());
            });
            var wG=agent.slice(agent.indexOf(")")+1).trim();
            var zX="";
            var qR=false;
            for(var i=0;i<wG.length;i++) {
                if(qR) {
                    zX+=wG[i]
                    if(wG[i]===")") {
                        zX
                        renderer.push(zX.trim());
                        zX="";
                        qR=false;
                    }
                } else if(zX.length<1&&wG[i]==="(") {
                    qR=true;
                    zX="("
                } else if(wG[i]===" ") {
                    if(zX.length>0) {
                        other.push(zX.trim());
                        zX="";
                    }
                } else {
                    zX+=wG[i];
                }
            }
            if(zX.length>0) {
                if(zX.startsWith("(")) {
                    renderer.push(zX.trim());
                } else {
                    other.push(zX.trim());
                }
            }
        } else {
            agent.split(" ").slice(1).forEach(o=>{
                other.push(o.trim())
            });
        }
        var data={
            browserName: null,
            browserVersion: null,
            browserBrand: null,
            deviceName: null,
            deviceBrand: null,
            processorName: null,
            processorBrand: null,
            platform: null,
            osName: null,
            osVersion: null,
            osBrand: null,
            osFamily: null,
            rendererName: null,
            rendererVersion: null,
            deviceType: 'desktop'
        };
        if(product==="dillo"||product==="Dillo") {
            data.browserName="dillo";
            data.browserBrand="dillo";
            data.browserVersion=productVersion;
            data.rendererName="dillo";
        } else if(product==="Opera") {
            if(find("Nintendo Wii",systemInformation)) {
                data.browserName="internetchannel";
                data.browserBrand="nintendo";
            } else {
                data.browserName="opera";
                data.browserName="opera";
            }
            data.browserVersion=productVersion;
            data.rendererName="presto";
            if(find("Presto/",other)) {
                data.rendererVersion=find("Presto/",other).split("/")[1];
            }
        } else if(find("Edg/",other)&&find("Chrome/",other)) {
            data.browserName="edge";
            data.browserBrand="microsoft";
            data.browserVersion=find("Edg/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("Electron/",other)&&find("Chrome/",other)) {
            data.browserName="electronapp";
            data.browserBrand="electron";
            data.browserVersion=find("Electron/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("DuckDuckGo/",other)) {
            data.browserName="privacybrowser";
            data.browserBrand="duckduckgo";
            data.browserVersion=find("DuckDuckGo/",other).split("/")[1];
            data.rendererName=find("Chrome/",other)?"blink":"webkit";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("Focus/",other)&&find("Chrome/",other)) {
            data.browserName="firefoxfocus";
            data.browserBrand="mozilla";
            data.browserVersion=find("Focus/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("Border/",other)&&find("Chrome/",other)) {
            data.browserName="border";
            data.browserBrand="border";
            data.browserVersion=find("Border/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("UCBrowser/",other)&&find("Chrome/",other)) {
            data.browserName="ucbrowser";
            data.browserBrand="ucbrowser";
            data.browserVersion=find("UCBrowser/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("MSIE",systemInformation)) {
            data.browserName="internetexplorer";
            data.browserBrand="microsoft";
            data.browserVersion=find("MSIE",systemInformation).split(" ")[1];
            data.rendererName="trident";
        } else if(find("rv:",systemInformation)
                 &&find("Trident/",systemInformation)) {
            data.browserName="internetexplorer";
            data.browserBrand="microsoft";
            data.browserVersion=find("rv:",systemInformation).split(":")[1];
            data.rendererName="trident";
        } else if(find("SamsungBrowser/",other)&&find("Chrome/",other)) {
            data.browserName="samsunginternet";
            data.browserBrand="samsung";
            data.browserVersion=find("SamsungBrowser/",other).split("/")[1];
            data.rendererName="blink";
        } else if(find("YaBrowser/",other)&&find("Chrome/",other)) {
            data.browserName="yandexbrowser";
            data.browserBrand="yandex";
            data.browserVersion=find("YaBrowser/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("Vivaldi/",other)&&find("Chrome/",other)) {
            data.browserName="vivaldi";
            data.browserBrand="vivaldi";
            data.browserVersion=find("Vivaldi/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("EdgA/",other)&&find("Chrome/",other)) {
            data.browserName="edge";
            data.browserBrand="microsoft";
            data.browserVersion=find("EdgeA/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("Version/",other)&&find("Android",systemInformation)) {
            data.browserName="androidbrowser";
            data.browserBrand="google";
            data.browserVersion=find("Version/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("Chrome/",other)) {
            data.browserName="chrome";
            data.browserBrand="google";
            data.browserVersion=find("Chrome/",other).split("/")[1];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("Tesla/",other)) {
            data.browserName="teslabrowser";
            data.browserBrand="tesla";
            data.browserVersion=(find("Tesla/",other).split("/")[1]||"0").split(".")[0];
            data.rendererName="blink";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("NintendoBrowser/",other)) {
            data.browserName="nintendobrowser";
            data.browserBrand="nintendo";
            data.browserVersion=(find("NintendoBrowser/",other).split("/")[1]||"0").split(".")[0];
            data.rendererName="webkit";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("NX/",other)) {
            data.browserName="nx";
            data.browserBrand="netfront";
            data.browserVersion=(find("NX/",other).split("/")[1]||"0").split(".")[0];
            data.rendererName="webkit";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("Edge/",other)) {
            data.browserName="edgelegacy";
            data.browserBrand="microsoft";
            data.browserVersion=find("Edge/",other).split("/")[1];
            data.rendererName="edgehtml";
        } else if(find("Safari/",other)&&find("Version/",other)&&(
            find("iPhone OS",systemInformation)
            ||find("iPad OS",systemInformation)
            ||find("Mac OS",systemInformation)
        )) {
            data.browserName="safari";
            data.browserBrand="apple";
            data.browserVersion=find("Version/",other).split("/")[1];
            data.rendererName="webkit";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("CriOS/",other)) {
            data.browserName="chrome";
            data.browserBrand="google";
            data.browserVersion=find("CriOS/",other).split("/")[1];
            data.rendererName="webkit";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("FxiOS/",other)) {
            data.browserName="firefox";
            data.browserBrand="mozilla";
            data.browserVersion=find("FxiOS/",other).split("/")[1];
            data.rendererName="webkit";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("EdgiOS/",other)) {
            data.browserName="edge";
            data.browserBrand="microsoft";
            data.browserVersion=find("EdgiOS/",other).split("/")[1];
            data.rendererName="webkit";
            if(find("AppleWebKit/",other)) {
                data.rendererVersion=find("AppleWebKit/",other).split("/")[1];
            }
        } else if(find("MyPal/",other)) {
            data.browserName="mypal";
            data.browserBrand="palemoon";
            data.browserVersion=find("MyPal/",other).split("/")[1];
            data.rendererName="gecko";
        } else if(find("Firefox/",other)) {
            data.browserName="firefox";
            data.browserBrand="mozilla";
            data.browserVersion=find("Firefox/",other).split("/")[1];
            data.rendererName="gecko";
        } else if(find("Xillo/",other)) {
            data.browserName="mirrazzweb";
            data.browserBrand="mirrazz";
            data.browserVersion=find("Xillo/",other).split("/")[1];
            data.rendererName="xillo";
        } else if(find("AppleWebKit/",other)) {
            data.browserName="webkitbased";
            data.browserVersion=find("AppleWebKit/",other).split("/")[1];
            data.rendererName="webkit";
        }
        if(systemInformation.includes("x64")) {
            data.platform="x64";
        } else if(systemInformation.includes("x86")) {
            data.platform="x86";
        } else if(systemInformation.includes("armvl7")) {
            data.platform="armvl7";
        } else if(systemInformation.includes("WOW64")) {
            data.platform="x64";
        } else if(systemInformation.includes("x16")) {
            data.platform="x16";
        }
        if(find("Intel Mac OS",systemInformation)) {
            data.processorName="intel";
            data.processorBrand="intel";
        } else if(find("M1 Mac OS",systemInformation)) {
            data.processorName="m1";
            data.processorBrand="apple";
        }
        if(systemInformation.includes("Nexus 5")) {
            data.deviceName="nexus5";
            data.deviceBrand="lg";
            data.deviceType='mobile';
        } else if(systemInformation.includes("Macintosh")) {
            data.deviceName="macintosh";
            data.deviceBrand="apple";
            data.deviceType='desktop';
        } else if(systemInformation.includes("iPod touch")) {
            data.deviceName="ipodtouch";
            data.deviceBrand="apple";
            data.deviceType='mobile';
        } else if(systemInformation.includes("Nintendo 3DS")) {
            data.deviceName="3ds";
            data.deviceBrand="nintendo";
            data.deviceType='mobileconsole';
        } else if(systemInformation.includes("New Nintendo 3FS")) {
            data.deviceName="new3ds";
            data.deviceBrand="nintendo";
            data.deviceType='mobileconsole';
        } else if(systemInformation.includes("Nintendo Wii")) {
            data.deviceName="wii";
            data.deviceBrand="nintendo";
            data.deviceType='gamingconsole';
        } else if(systemInformation.includes("iPhone")) {
            data.deviceName="iphone";
            data.deviceBrand="apple";
            data.deviceType='mobile';
        } else if(systemInformation.includes("iPad")) {
            data.deviceName="ipad";
            data.deviceBrand="apple";
            data.deviceType='tablet';
        } else if(systemInformation.includes("XBOX_ONE_ED")) {
            data.deviceName="xboxone";
            data.deviceBrand="microsoft";
            data.deviceType='gamingconsole';
        } else if(systemInformation.includes("Xbox 360")) {
            data.deviceName="xbox360";
            data.deviceBrand="microsoft";
            data.deviceType='gamingconsole';
        } else if(systemInformation.includes("Xbox One")) {
            data.deviceName="xboxone";
            data.deviceBrand="microsoft";
            data.deviceType='gamingconsole';
        } else if(systemInformation.includes("Raspberry Pi 4")) {
            data.deviceName="rpi4";
            data.deviceBrand="raspberrypi";
            data.deviceType='desktop';
        } else if(systemInformation.includes("Raspberry Pi 3B")) {
            data.deviceName="rpi3b";
            data.deviceBrand="raspberrypi";
            data.deviceType='desktop';
        } else if(systemInformation.includes("Raspberry Pi 3A")) {
            data.deviceName="rpi3a";
            data.deviceBrand="raspberrypi";
            data.deviceType='desktop';
        } else if(systemInformation.includes("Raspberry Pi 2B")) {
            data.deviceName="rpi2b";
            data.deviceBrand="raspberrypi";
            data.deviceType='desktop';
        } else if(systemInformation.includes("Raspberry Pi 2A")) {
            data.deviceName="rpi2a";
            data.deviceBrand="raspberrypi";
            data.deviceType='desktop';
        } else if(systemInformation.includes("Raspberry Pi 1B")) {
            data.deviceName="rpib";
            data.deviceBrand="raspberrypi";
            data.deviceType='desktop';
        } else if(systemInformation.includes("Raspberry Pi 1A")) {
            data.deviceName="rpi";
            data.deviceBrand="raspberrypi";
            data.deviceType='desktop';
        } else if(systemInformation.includes("Raspberry Pi 400")) {
            data.deviceName="rpi400";
            data.deviceBrand="raspberrypi";
            data.deviceType='desktop';
        } else if(find("CrOS",systemInformation)) {
            data.deviceName="chromebook";
            data.deviceBrand="google";
            data.deviceType='desktop';
        } else if(systemInformation.includes("Surface Pro")) {
            data.deviceName="surfacepro";
            data.deviceBrand="microsoft";
            data.deviceType='tablet';
        } else if(systemInformation.includes("Yahoo! Slurp")) {
            data.deviceName="slurp";
            data.deviceBrand="yahoo";
            data.deviceType='crawler';
        } else if(find("Googlebot/",systemInformation)) {
            data.deviceName="googlebot";
            data.deviceBrand="google";
            data.deviceType='crawler';
        } else if(find("KFMUWI",systemInformation)) {
            data.deviceName="kindlefire7";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFTT",systemInformation)) {
            data.deviceName="kindlefirehd7";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFASWI",systemInformation)) {
            data.deviceName="kindlefirehd72014";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFSOWI",systemInformation)) {
            data.deviceName="kindlefirehd72013";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFFOWI",systemInformation)) {
            data.deviceName="kindlefire2015";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFAUWI",systemInformation)) {
            data.deviceName="kindlefire2017";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFOT",systemInformation)) {
            data.deviceName="kindlefire2ndgen";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFTBWI",systemInformation)) {
            data.deviceName="kindlefirehd102015";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFSUWI",systemInformation)) {
            data.deviceName="kindlefirehd102017";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFMAWI",systemInformation)) {
            data.deviceName="kindlefirehd102019";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFMEWI",systemInformation)) {
            data.deviceName="kindlefirehd82015";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFGIWI",systemInformation)) {
            data.deviceName="kindlefirehd82016";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFDOWI",systemInformation)) {
            data.deviceName="kindlefirehd82017";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFKAWI",systemInformation)) {
            data.deviceName="kindlefirehd82018";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFJWI",systemInformation)) {
            data.deviceName="kindlefirehd89";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFTHWI",systemInformation)) {
            data.deviceName="kindlefirehdx7";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFSAWI",systemInformation)) {
            data.deviceName="kindlefirehdx892014";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("KFAPWI",systemInformation)) {
            data.deviceName="kindlefirehdx892013";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("Kindle Fire",systemInformation)) {
            data.deviceName="kindlefire";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("BlackBerry",systemInformation)) {
            data.deviceName="blackberry";
            data.deviceBrand="blackberry";
            data.deviceType='mobile';
        } else if(find("Pixel 4XL",systemInformation)) {
            data.deviceName="pixel4xl";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel 4",systemInformation)) {
            data.deviceName="pixel4";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel 3",systemInformation)) {
            data.deviceName="pixel3";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel 5",systemInformation)) {
            data.deviceName="pixel5";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel 6",systemInformation)) {
            data.deviceName="pixel6";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel 7",systemInformation)) {
            data.deviceName="pixel7";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel 8",systemInformation)) {
            data.deviceName="pixel8";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel 9",systemInformation)) {
            data.deviceName="pixel9";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel classic",systemInformation)) {
            data.deviceName="pixelclassic";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel core",systemInformation)) {
            data.deviceName="pixelcore";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel corp",systemInformation)) {
            data.deviceName="pixelcorp";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Pixel",systemInformation)) {
            data.deviceName="pixel";
            data.deviceBrand="google";
            data.deviceType='mobile';
        } else if(find("Nexus 4",systemInformation)) {
            data.deviceName="nexus4";
            data.deviceBrand="lg";
            data.deviceType='mobile';
        } else if(find("Nexus 5X",systemInformation)) {
            data.deviceName="nexus5x";
            data.deviceBrand="lg";
            data.deviceType='mobile';
        } else if(find("LG-",systemInformation)) {
            data.deviceName="lg"+(find(
                "LG-",systemInformation
            ).split(" ")[0].slice(2).toLowerCase());
            data.deviceBrand="lg";
            data.deviceType='mobile';
        } else if(find("Nexus",systemInformation)) {
            data.deviceName="nexus";
            data.deviceBrand="lg";
            data.deviceType='mobile';
        } else if(find("DROID RAZR",systemInformation)) {
            data.deviceName="droidrazr";
            data.deviceBrand="motorola";
            data.deviceType='mobile';
        } else if(find("XT1254",systemInformation)) {
            data.deviceName="droidturbo";
            data.deviceBrand="motorola";
            data.deviceType='mobile';
        } else if(other.includes("Tesla")) {
            data.deviceName="tesladash";
            data.deviceBrand="tesla";
            data.deviceType='automobile';
        } else if(find("Tesla/",other)) {
            data.deviceName="tesladash";
            data.deviceBrand="tesla";
            data.deviceType='automobile';
        } else if(find("Tesla",systemInformation)) {
            data.deviceName="tesladash";
            data.deviceBrand="tesla";
            data.deviceType='automobile';
        } else if(find("PLAYSTATION 3",systemInformation)) {
            data.deviceName="ps3";
            data.deviceBrand="sony";
            data.deviceType='gamingconsole';
        } else if(find("PlayStation 4",systemInformation)) {
            data.deviceName="ps4";
            data.deviceBrand="sony";
            data.deviceType='gamingconsole';
        } else if(find("PlayStation 5",systemInformation)) {
            data.deviceName="ps4";
            data.deviceBrand="sony";
            data.deviceType='gamingconsole';
        } else if(find("PlayStation",systemInformation)) {
            data.deviceName="playstation";
            data.deviceBrand="sony";
            data.deviceType='gamingconsole';
        } else if(find("Kindle/",other)) {
            data.deviceName="kindle";
            data.deviceBrand="amazon";
            data.deviceType='tablet';
        } else if(find("Tizen",systemInformation)&&
                 find("SMART-TV",systemInformation)) {
            data.deviceName="qled4k";
            data.deviceBrand="samsung";
            data.deviceType='tv';
        } else if(find("WebOS",systemInformation)
                 &&find("Linux/SmartTV",systemInformation)) {
            data.deviceName="oled4k";
            data.deviceBrand="lg";
            data.deviceType='tv';
        } else if(find("Glass 1",systemInformation)) {
            data.deviceName="glass";
            data.deviceBrand="google";
            data.deviceType='wearables';
        } else if(product==="DuckDuckBot") {
            data.deviceName="duckduckbot";
            data.deviceBrand="duckduckgo";
            data.deviceType='crawler';
        }
        if(find("Glass 1",systemInformation)) {
            data.osName="glassos";
            data.osBrand="google";
            data.osVersion="7";
            data.osFamily="linux";
        } else if(find("iPhone OS",systemInformation)) {
            data.osName="ios";
            data.osBrand="apple";
            find(
                "iPhone OS",systemInformation
            ).slice(find(
                "iPhone OS",systemInformation
            ).indexOf("iPhone OS")).split(" ")[3].split("_").join(".");
            data.osFamily="osx";
        } else if(find("iPad OS",systemInformation)) {
            data.osName="ios";
            data.osBrand="apple";
            data.osVersion=find(
                "iPad OS",systemInformation
            ).slice(find(
                "iPad OS",systemInformation
            ).indexOf("iPad OS")).split(" ")[3].split("_").join(".");
            data.osFamily="osx";
        } else if(find("Mac OS X",systemInformation)) {
            data.osName="macos";
            data.osBrand="apple";
            data.osVersion=find(
                "Mac OS X",systemInformation
            ).slice(find(
                "Mac OS X",systemInformation
            ).indexOf("Mac OS X")).split(" ")[3].split("_").join(".");
            data.osFamily="osx";
        } else if(find("Mac OS",systemInformation)) {
            data.osName="macos";
            data.osBrand="apple";
            data.osVersion=find(
                "Mac OS",systemInformation
            ).slice(find(
                "Mac OS",systemInformation
            ).indexOf("Mac OS")).split(" ")[3].split("_").join(".");
            data.osFamily="osx";
        } else if(find("Windows 95",systemInformation)) {
            data.osName="windows95";
            data.osBrand="microsoft";
            data.osVersion="95";
            data.osFamily="win9x";
        } else if(find("Windows 98",systemInformation)) {
            data.osName="windows98";
            data.osBrand="microsoft";
            data.osVersion="98";
            data.osFamily="win9x";
        } else if(find("Windows 3.1",systemInformation)) {
            data.osName="windows3.1";
            data.osBrand="microsoft";
            data.osVersion="3.1";
            data.osFamily="dos";
        } else if(find("MS-DOS",systemInformation)) {
            data.osName="msdos";
            data.osBrand="microsoft";
            data.osVersion=find("MS-DOS",systemInformation).split(" ")[1];
            data.osFamily="dos";
        } else if(find("Android",systemInformation)
                 &&find("KF",systemInformation)) {
            data.osName="fireos";
            data.osBrand="amazon";
            data.osVersion="3";
            data.osFamily="android";
        } else if(find("Kindle Fire",systemInformation)) {
            data.osName="fireos";
            data.osBrand="amazon";
            data.osVersion="3";
            data.osFamily="android";
        } else if(find("Kindle/",other)) {
            data.osName="fireos";
            data.osBrand="amazon";
            data.osVersion="3";
            data.osFamily="anroid";
        } else if(find("Android",systemInformation)) {
            data.osName="android";
            data.osBrand="google";
            data.osVersion=find(
                "Android",systemInformation
            ).split(" ")[1];
            data.osFamily="android";
        } else if(find("Googlebot",systemInformation)) {
            data.osName="googlebot";
            data.osBrand="google";
            data.osVersion="0";
            data.osFamily="linux";
        } else if(find("Windows Me",systemInformation)) {
            data.osName="windowsme";
            data.osBrand="microsoft";
            data.osVersion="2000";
            data.osFamily="win9x";
        } else if(find("Raspbian",systemInformation)) {
            data.osName="pios";
            data.osBrand="raspberrypi";
            data.osVersion="5";
            data.osFamily="linux";
        } else if(find("Ubuntu",systemInformation)) {
            data.osName="ubuntu";
            data.osBrand="ubuntu";
            data.osVersion=find('Ubuntu',systemInformation).split(" ")[1];
            data.osFamily="linux";
        } else if(find("Nintendo Switch",systemInformation)) {
            data.osName="switchos";
            data.osBrand="nintendo";
            data.osVersion="1";
            data.osFamily="unknown";
        } else if(find("Nintendo 3DS",systemInformation)) {
            data.osName="3dsos";
            data.osBrand="nintendo";
            data.osVersion="1";
            data.osFamily="unknown";
        } else if(find("Nintendo Wii",systemInformation)) {
            data.osName="wiios";
            data.osBrand="nintendo";
            data.osVersion="1";
            data.osFamily="unknown";
        } else if(find("Manjaro",systemInformation)) {
            data.osName="manjaro";
            data.osBrand="kde";
            data.osVersion=find('Manjaro',systemInformation).split(" ")[1];
            data.osFamily="linux";
        } else if(find("KDE Plasma",systemInformation)) {
            data.osName="plasma";
            data.osBrand="kde";
            data.osVersion=find('KDE Plasma',systemInformation).split(" ")[1];
            data.osFamily="linux";
        } else if(find("Debian",systemInformation)) {
            data.osName="debian";
            data.osBrand="debian";
            data.osVersion=find('debian',systemInformation).split(" ")[1];
            data.osFamily="linux";
        } else if(find("CrOS",systemInformation)) {
            data.osName="chromeos";
            data.osBrand="google";
            data.osVersion="103";
            data.osFamily="linux";
        } else if(find("MzOS",systemInformation)) {
            data.osName="mirrazzos";
            data.osBrand="mirrazz";
            data.osVersion=find('MzOS',systemInformation).split(" ")[1];
            data.osFamily="linux";
        } else if(find("Windows 11",systemInformation)) {
            data.osName="windows11";
            data.osBrand="microsoft";
            data.osVersion='11';
            data.osFamily="winnt";
        } else if(find("Tizen",systemInformation)) {
            data.osName="tizen";
            data.osBrand="samsung";
            data.osVersion='1.0';
            data.osFamily="linux";
        } else if(find("WebOS",systemInformation)) {
            data.osName="webos";
            data.osBrand="lg";
            data.osVersion="webos";
            data.osFamily="linux";
        } else if(find("Windows NT",systemInformation)) {
            data.osName=ntversiontoosversion(
                find("Windows NT",systemInformation).split(" ")[2]||"0"
            );
            data.osBrand="microsoft";
            data.osVersion=find("Windows NT",systemInformation).split(" ")[2];
            data.osFamily="winnt";
        } else if(find("Windows",systemInformation)) {
            data.osName="windows"
            data.osBrand="microsoft";
            data.osFamily="win32";
        } else if(find("Linux",systemInformation)) {
            data.osName="linux"
            data.osBrand="linux";
            data.osFamily="linux";
        } else if(find("UNIX",systemInformation)) {
            data.osName="unix"
            data.osBrand="unix";
            data.osFamily="unix";
        }
        return data
    }
}

function ntversiontoosversion(ntv) {
    if(ntv=="5.0") {
        return 'windows2000'
    } else if(ntv=="5.1"||ntv=='5.2') {
        return 'windowsxp'
    } else if(ntv=='6.0') {
        return 'windowsvista'
    } else if(ntv=='6.1') {
        return 'windows7'
    } else if(ntv=='6.2') {
        return 'windows8'
    } else if(ntv=='6.3') {
        return 'windows8.1'
    } else if(ntv=='10.0') {
        return 'windows10'
    } else if(ntv=='4.0') {
        return 'ntworkstation4'
    } else if(ntv=='3.1') {
        return 'ntworkstation3'
    } else {
        return 'windowsnt'
    }
}

function find(data,o) {
    for(var i=0;i<o.length;i++) {
        if(o[i].includes(data)) {
            return o[i]
        }
    }
    return null;
}

module.exports=parser
